import 'package:flutter/material.dart';
import 'package:peliculas/src/providers/peliculas.provider.dart';
import 'package:peliculas/src/widgets/card_swiper_widget.dart';
import 'package:peliculas/src/widgets/card_swiper_widget2.dart';

class HomePage extends StatelessWidget {
  final peliculasProvider = new PeliculasProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Peliculas en Cines'),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.search), onPressed: null)
          ],
        ),
        //body: SafeArea(child: Text('Hola Mundo')));
        body: Container(
            child: Column(
          children: <Widget>[
            _swipperTarjetas(),
            _swipperTarjetashorizontal()
            ],

        )));
  }

  Widget _swipperTarjetas() {

    return FutureBuilder(
      future:  peliculasProvider.getEnCines(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData){
          return CardSwiper(peliculas: snapshot.data);
        }
        else{
          return Container(
            height: 400.0,
            child: Center(
              child: CircularProgressIndicator()
            )
          ); 
        }
      },
    );

    

    // peliculasProvider.getEnCines();

    // return CardSwiper(peliculas: [1, 2, 3, 4, 5]);

    //return Container();
  }

  Widget _swipperTarjetashorizontal() {

    return FutureBuilder(
      future:  peliculasProvider.getPopulares(),
      builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
        if(snapshot.hasData){
          return CardSwiperHorizontal(peliculas: snapshot.data);
        }
        else{
          return Container(
            height: 400.0,
            child: Center(
              child: CircularProgressIndicator()
            )
          ); 
        }
      },
    );
  }
}
