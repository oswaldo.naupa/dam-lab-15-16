import 'package:http/http.dart' as http;

import 'dart:convert';

import 'package:peliculas/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = 'cd6bba8bb290f867e1c2af3f900066c7';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';

  Future <List<Pelicula>> getEnCines() async {

  final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key': _apikey,
      'language': _language
    });

  final resp = await http.get(url);

  final decodedData = json.decode(resp.body);

  final peliculas = new Peliculas.fromJsonList(decodedData['results']);

  //print(decodedData['results']);

  //print(peliculas.items[0].title);


  //return [];
    return peliculas.items;
  }

  Future <List<Pelicula>> getPopulares() async {

  final url = Uri.https(_url, '3/movie/popular', {
      'api_key': _apikey,
      'language': _language
    });

  final resp = await http.get(url);

  final decodedData = json.decode(resp.body);

  final peliculas = new Peliculas.fromJsonList(decodedData['results']);

  //print(decodedData['results']);

  //print(peliculas.items[0].title);


  //return [];
    return peliculas.items;
  }

}
