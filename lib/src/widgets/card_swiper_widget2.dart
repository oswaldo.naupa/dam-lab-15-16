import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:peliculas/src/models/pelicula_model.dart';

class CardSwiperHorizontal extends StatelessWidget {
  final List<Pelicula> peliculas;

  CardSwiperHorizontal({@required this.peliculas});

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    /*return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: Swiper(
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,

        layout: SwiperLayout.STACK,

        itemBuilder: (BuildContext context, int index) {
          return new Image.network(
            "http://via.placeholder.com/350x150",
            fit: BoxFit.fill,
          );
        },
        itemCount: 3,
        //pagination: new SwiperPagination(),
        //control: new SwiperControl(),
      ),
    );*/

    return Container(
      padding: EdgeInsets.only(top: 100.0),
      child: Swiper(
        itemWidth: _screenSize.width * 0.25,
        itemHeight: _screenSize.height * 0.27,
  
        layout: SwiperLayout.CUSTOM,
          autoplay: true,
         customLayoutOption: new CustomLayoutOption(
              startIndex: -1,
              stateCount: 3
          ).addTranslate([
            new Offset(-140.0, 0),
            new Offset(0.0, 0.0),
            new Offset(140.0, 0)
          ]),

        itemBuilder: (BuildContext context, int index) {
          return 
          Column(
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
                /*child: Image.network(
                "http://via.placeholder.com/350x150",
                fit: BoxFit.cover,
              ) */
              child: 
              FadeInImage(
                image: NetworkImage(peliculas[index].getPosterImg()),
                placeholder: AssetImage('assets/no-image.jpg'),
                fit: BoxFit.fill,
              ),
            ),
            Text( peliculas[index].getPosterText()),
            ]
          );
        },
        itemCount: peliculas.length,
        
          //control: new SwiperControl(),
      ),
    );
  }
}